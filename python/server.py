import os
import sys
import time
import re
import argparse
from concurrent import futures
import subprocess

#gRPC server stuff
import grpc
import robohon_message_pb2
import robohon_message_pb2_grpc

from action_dict import action_dict


class Servicer(robohon_message_pb2_grpc.RoBoHoNMessageServicer):
	def RequestInfo(self, request, context):
		global input_s
		global response
		if request.info_type not in ['idle','finish']:
			response = request.info_type
		if input_s == None:
			sentence = 'empty'   
		else:
			sentence = input_s
			if request.info_type == "idle":
				input_s = 'empty'
			else:
				input_s = None
		return robohon_message_pb2.desktop(sentence=sentence)

class SpeechRoutine(object):
	def __init__(self):
		#Speech and motion data is taken from here
		self.action_dict = action_dict()

		#Data will be stored in this dictionary
		self.current_user = {}

		#Start gRPC server
		self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
		robohon_message_pb2_grpc.add_RoBoHoNMessageServicer_to_server(Servicer(), self.server)
		self.server.add_insecure_port('[::]:' + str(args.server_port))
		print('server started on port ' + str(args.server_port))
		self.server.start()
		
	def startdemo(self):
		global input_s
		global response
		result = 'test'
		
		self.do_action('demo')

		self.server.stop(0)
		return

	def do_action(self, action_key):
		global response
		response_cat = ''
		i = 0
		setback = 0

		action_values = self.action_dict.get(action_key)
		
		while i < len(action_values):
			item = action_values[i]
			
			#Classify response and get the move meta
			if type(item) is list:     
				response_cat = self.classify_response(response)
				#print(response_cat)
				try:
					item = list(filter(lambda x: response_cat in x, item))[0]
					item = item.replace(response_cat, '')
				except IndexError:
					item = '#Lets try again'
					setback = -2
				if 'move' in item:
					setback = int(item[4:6]) - 1
					item = item[6:]
			
			#Replace keywords
			categories = re.findall('[@]\w+', item) #variable category
			for category in categories:	
				#Use this if you dont wanna store the value, 
				#just make the robot repeat the last resposne in a sentence
				if '@keyword' in category:
					item = item.replace('@keyword', response)
				
				if '@set_' in category:
					category = category[5:]
					if category in ['name', 'age', 'gender']:
						if category == 'age' and not self.is_int(response):
							item = '#Please say a number'
							setback = -2
						if category == 'gender' and response not in ['boy', 'girl']:	
							item = '#Please say boy or girl'
							setback = -2
						self.current_user.update({category:response})
					item = item.replace('@set_' + category, response)
				
				if '@get_' in category:
					category = category[5:]
					print(category)
					if category in ['name', 'age', 'gender']:
						response = self.current_user[category]
					
					item = item.replace('@get_' + category, response)
			
			self.say(item)
			i += 1
			i += setback
			setback = 0

		return response_cat

	def say(self, item):
		global input_s

		input_s = 'm' + item
		print(input_s)

		# Wait for response
		while input_s != None:
			time.sleep(0.1)
		return

	def classify_response(self, input_s):
		positive_responses = ["yes", "okay", "sure"]
		negative_responses = ["no", "do_not", "don't", "i_don't_want", "i_don't_know"]
		
		if input_s in positive_responses:
			return 'positive'
		elif input_s in negative_responses:
			return 'negative'
		else:
			return 'default'

if __name__ == '__main__':
	global input_s
	global response

	parser = argparse.ArgumentParser()
	parser.add_argument('--server_port', type=str, default=50051, help='Server port')
	args = parser.parse_args()

	input_s = None
	response = 'test'

	sr = SpeechRoutine()
	sr.startdemo()