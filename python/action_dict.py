# -*- coding: UTF-8 -*-

def action_dict():
    action = {}

    action['demo'] = ['cont#Hello, my name is robo-hon',
    'getreply#What is your name?',
    ['defaultgetreply#You said @set_name, is that right?'],
    ['positive#Great! @get_name, nice to meet you','negativemove-2#Lets try again','defaultmove-2#Lets try again'],
    'getreply#Would you like to see me dance?',
    ['positive#Okay, I will dance now','negativemove+2#That is too bad','defaultmove-1#Please say yes or no'],
    'randomdance',
    'cont#I want to sit down',
    'sitdown',
    'cont#bye bye!']


    return action