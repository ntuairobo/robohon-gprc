package com.example.airobo.remotecontrol.util;

import jp.co.sharp.android.robot.RobotServoManager;
import jp.co.sharp.android.robot.ServoPosition;

/**
 * Created by sarahchiu on 9/18/17.
 */

public class ServoMotor {
    //Servo manager
    private RobotServoManager mRobotServoManager;

    private int[] servo = {0,140,140,0,54,-54,-270,350,0,270,-350,-140,-140,0};

    public ServoMotor(RobotServoManager rsm) {
        mRobotServoManager = rsm;
    }

    //Set servo positions general method
    public void setServos(int[] servoV, int timeMs) {
        ServoPosition RLeg = new ServoPosition();
        RLeg.servo_id = RobotServoManager.SERVO_LEG_TOP_RIGHT;
        ServoPosition RKnee = new ServoPosition();
        RKnee.servo_id = RobotServoManager.SERVO_LEG_BOTTOM_RIGHT;
        ServoPosition RFoot = new ServoPosition();
        RFoot.servo_id = RobotServoManager.SERVO_FOOT_RIGHT;
        ServoPosition LHead = new ServoPosition();
        LHead.servo_id = RobotServoManager.SERVO_HEAD_LEFT;
        ServoPosition RHead = new ServoPosition();
        RHead.servo_id = RobotServoManager.SERVO_HEAD_RIGHT;
        ServoPosition RArm = new ServoPosition();
        RArm.servo_id = RobotServoManager.SERVO_ARM_RIGHT;
        ServoPosition LShoulder = new ServoPosition();
        LShoulder.servo_id = RobotServoManager.SERVO_SHOULDER_LEFT;
        ServoPosition Neck = new ServoPosition();
        Neck.servo_id = RobotServoManager.SERVO_NECK;
        ServoPosition RShoulder = new ServoPosition();
        RShoulder.servo_id = RobotServoManager.SERVO_SHOULDER_RIGHT;
        ServoPosition LArm = new ServoPosition();
        LArm.servo_id = RobotServoManager.SERVO_ARM_LEFT;
        ServoPosition LLeg = new ServoPosition();
        LLeg.servo_id = RobotServoManager.SERVO_LEG_TOP_LEFT;
        ServoPosition LKnee = new ServoPosition();
        LKnee.servo_id = RobotServoManager.SERVO_LEG_BOTTOM_LEFT;
        ServoPosition LFoot = new ServoPosition();
        LFoot.servo_id = RobotServoManager.SERVO_FOOT_LEFT;

        RLeg.pos = servoV[1];
        RKnee.pos = servoV[2];
        RFoot.pos = servoV[3];
        LHead.pos = servoV[4];
        RHead.pos = servoV[5];
        LShoulder.pos = servoV[6];
        RArm.pos = servoV[7];
        Neck.pos = servoV[8];
        RShoulder.pos = servoV[9];
        LArm.pos = servoV[10];
        LLeg.pos = servoV[11];
        LKnee.pos = servoV[12];
        LFoot.pos = servoV[13];

        mRobotServoManager.startServoControl();
        mRobotServoManager.setServoPosition(new ServoPosition[]{
                RLeg, RKnee, RFoot, LHead, RHead, LShoulder, RArm, Neck, RShoulder, LArm, LLeg, LKnee, LFoot
        }, timeMs);
        mRobotServoManager.stopServoControl();
    }

    public ServoPosition[] getServoPosition() {
        ServoPosition RLeg = new ServoPosition();
        RLeg.servo_id = RobotServoManager.SERVO_LEG_TOP_RIGHT;
        ServoPosition RKnee = new ServoPosition();
        RKnee.servo_id = RobotServoManager.SERVO_LEG_BOTTOM_RIGHT;
        ServoPosition RFoot = new ServoPosition();
        RFoot.servo_id = RobotServoManager.SERVO_FOOT_RIGHT;
        ServoPosition LHead = new ServoPosition();
        LHead.servo_id = RobotServoManager.SERVO_HEAD_LEFT;
        ServoPosition RHead = new ServoPosition();
        RHead.servo_id = RobotServoManager.SERVO_HEAD_RIGHT;
        ServoPosition RArm = new ServoPosition();
        RArm.servo_id = RobotServoManager.SERVO_ARM_RIGHT;
        ServoPosition LShoulder = new ServoPosition();
        LShoulder.servo_id = RobotServoManager.SERVO_SHOULDER_LEFT;
        ServoPosition Neck = new ServoPosition();
        Neck.servo_id = RobotServoManager.SERVO_NECK;
        ServoPosition RShoulder = new ServoPosition();
        RShoulder.servo_id = RobotServoManager.SERVO_SHOULDER_RIGHT;
        ServoPosition LArm = new ServoPosition();
        LArm.servo_id = RobotServoManager.SERVO_ARM_LEFT;
        ServoPosition LLeg = new ServoPosition();
        LLeg.servo_id = RobotServoManager.SERVO_LEG_TOP_LEFT;
        ServoPosition LKnee = new ServoPosition();
        LKnee.servo_id = RobotServoManager.SERVO_LEG_BOTTOM_LEFT;
        ServoPosition LFoot = new ServoPosition();
        LFoot.servo_id = RobotServoManager.SERVO_FOOT_LEFT;

        ServoPosition[] list = new ServoPosition[]{
                RLeg, RKnee, RFoot, LHead, RHead, LShoulder, RArm, Neck, RShoulder, LArm, LLeg, LKnee, LFoot
        };
        mRobotServoManager.getServoPosition(list);
        return list;
    }

    public void HomePosture() {
        servo[1] = 140;
        servo[2] = 140;
        servo[3] = 0;
        servo[4] = 54;
        servo[5] = -54;
        servo[6] = -270;
        //servo[7] = 350;
        servo[7] = 300;
        servo[8] = 0;
        servo[9] = 270;
        //servo[10] = -350;
        servo[10] = -300;
        servo[11] = -140;
        servo[12] = -140;
        servo[13] = 0;
        setServos(servo, 1000);
    }

    public void Hysterical() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[11] = 219;
        servo[1] = -223;
        servo[12] = 96;
        servo[2] = -116;

        for (int i = 0; i < 2; i++) {
            servo[8] = -191;
            servo[4] = -53;
            servo[5] = 103;
            servo[6] = 167;
            servo[9] = 403;
            servo[10] = -470;
            servo[7] = 314;

            setServos(servo, 250);

            servo[8] = 202;
            servo[4] = -57;
            servo[5] = 12;
            servo[6] = -371;
            servo[9] = -139;
            servo[10] = -331;
            servo[7] = 469;

            setServos(servo, 250);
        }

        HomePosture();
    }

    public void Excited() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[11] = 219;
        servo[1] = -223;
        servo[12] = 96;
        servo[2] = -116;

        servo[6] = 171;
        servo[9] = -170;
        servo[10] = -628;
        servo[7] = 610;

        setServos(servo, 400);

        servo[6] = 171;
        servo[9] = -204;
        servo[10] = -336;
        servo[7] = 336;

        setServos(servo, 150);

        servo[1] = 140;
        servo[2] = 140;
        servo[6] = 703;
        servo[7] = 330;
        servo[9] = -690;
        servo[10] = -332;
        servo[11] = -140;
        servo[12] = -140;

        setServos(servo, 400);

        for (int i = 0; i < 2; i++) {
            servo[6] = 693;
            servo[9] = -678;
            servo[10] = -882;
            servo[7] = 886;

            setServos(servo, 100);

            servo[6] = 703;
            servo[9] = -690;
            servo[10] = -332;
            servo[7] = 330;

            setServos(servo, 100);
        }

        HomePosture();
    }

    public void Happy() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[6] = 16;
        servo[9] = -25;
        servo[10] = -453;
        servo[7] = 504;

        setServos(servo, 100);

        servo[6] = 1277;
        servo[9] = -1236;
        servo[10] = -436;
        servo[7] = 505;

        setServos(servo, 100);

        servo[10] = 34;
        servo[7] = -49;

        setServos(servo, 100);

        HomePosture();
    }

    public void Happy2() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = 359;
        servo[4] = 108;
        servo[5] = 16;
        servo[9] = -1286;
        servo[7] = 298;

        setServos(servo, 150);

        servo[7] = -60;

        setServos(servo, 50);

        HomePosture();
    }

    public void Interested() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = 279;
        servo[4] = -58;
        servo[5] = 23;
        servo[9] = -811;
        servo[7] = 878;

        setServos(servo, 500);

        HomePosture();
    }

    public void Interested2() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = -262;
        servo[4] = 71;
        servo[5] = -49;
        servo[6] = 805;
        servo[9] = 20;
        servo[10] = -874;
        servo[7] = 481;

        setServos(servo, 500);

        HomePosture();
    }

    public void Laugh() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[6] = 121;
        servo[9] = -492;
        servo[10] = -491;
        servo[7] = 177;
        servo[11] = 219;
        servo[1] = -223;
        servo[12] = 96;
        servo[2] = -116;

        setServos(servo, 250);

        servo[8] = -2;
        servo[4] = -54;
        servo[5] = -66;
        servo[9] = -125;
        servo[7] = 571;

        setServos(servo, 150);

        servo[8] = -49;
        servo[4] = -57;
        servo[5] = 78;
        servo[9] = -492;
        servo[7] = 177;

        setServos(servo, 150);

        servo[9] = -125;
        servo[7] = 571;

        setServos(servo, 100);

        servo[8] = -79;
        servo[4] = -52;
        servo[5] = 125;
        servo[9] = -492;
        servo[7] = 177;

        setServos(servo, 150);

        servo[9] = -125;
        servo[7] = 571;

        setServos(servo, 100);

        servo[9] = 23;
        servo[7] = 476;

        setServos(servo, 100);

        servo[8] = 194;
        servo[4] = -64;
        servo[5] = 49;
        servo[11] = 422;
        servo[1] = -410;
        servo[12] = 213;
        servo[2] = -222;

        setServos(servo, 150);

        servo[8] = -174;
        servo[4] = -58;
        servo[5] = 44;

        setServos(servo, 100);

        servo[8] = 194;
        servo[4] = -64;
        servo[5] = 49;

        setServos(servo, 100);

        servo[8] = -174;
        servo[4] = -58;
        servo[5] = 44;

        setServos(servo, 100);

        HomePosture();
    }

    public void Laugh2() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[6] = -34;
        servo[9] = 14;
        servo[10] = -487;
        servo[7] = 520;
        servo[11] = 219;
        servo[1] = -223;
        servo[12] = 96;
        servo[2] = -116;

        setServos(servo, 250);

        for (int i = 0; i < 2; i++) {
            servo[8] = 194;
            servo[4] = -64;
            servo[5] = 49;

            setServos(servo, 100);

            servo[8] = -174;
            servo[4] = -58;
            servo[5] = 44;

            setServos(servo, 100);
        }

        HomePosture();
    }

    public void Mocker() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = -29;
        servo[4] = -19;
        servo[5] = 63;
        servo[6] = 529;
        servo[9] = 14;
        servo[10] = -362;
        servo[7] = 498;
        servo[11] = 219;
        servo[1] = -223;
        servo[12] = 96;
        servo[2] = -116;

        setServos(servo, 250);

        servo[6] = 741;
        servo[10] = -128;
        servo[11] = 422;
        servo[1] = -410;
        servo[12] = 213;
        servo[2] = -222;

        setServos(servo, 150);

        HomePosture();
    }

    public void Proud() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = 329;
        servo[4] = 136;
        servo[5] = -145;
        servo[6] = -378;
        servo[9] = 401;
        servo[10] = -201;
        servo[7] = 194;

        setServos(servo, 1000);

        HomePosture();
    }

    public void Shy() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = 276;
        servo[4] = -57;
        servo[5] = 0;
        servo[6] = 403;
        servo[9] = -248;
        servo[10] = -846;
        servo[7] = 794;

        setServos(servo, 500);

        for (int i = 0; i < 2; i++) {
            servo[6] = 300;
            servo[9] = -389;
            servo[10] = -799;
            servo[7] = 794;

            setServos(servo, 250);

            servo[6] = 403;
            servo[9] = -248;
            servo[10] = -846;
            servo[7] = 794;

            setServos(servo, 250);
        }

        HomePosture();
    }

    public void Winner() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[6] = 1235;
        servo[9] = -1282;
        servo[10] = -226;
        servo[7] = 205;

        setServos(servo, 500);

        for (int i = 0; i < 2; i++) {
            servo[6] = 827;
            servo[9] = -806;
            servo[10] = -345;
            servo[7] = 323;

            setServos(servo, 250);

            servo[6] = 1235;
            servo[9] = -1282;
            servo[10] = -226;
            servo[7] = 205;

            setServos(servo, 250);
        }

        HomePosture();
    }

    public void Angry() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[11] = 219;
        servo[1] = -223;
        servo[12] = 96;
        servo[2] = -116;

        servo[6] = 328;
        servo[9] = -326;
        servo[10] = -337;
        servo[7] = 337;

        setServos(servo, 500);

        servo[6] = -285;
        servo[9] = 335;

        setServos(servo, 150);

        for (int i = 0; i < 2; i++) {
            servo[8] = -174;
            servo[4] = -58;
            servo[5] = 44;

            setServos(servo, 100);

            servo[8] = 194;
            servo[4] = -64;
            servo[5] = 49;

            setServos(servo, 100);
        }

        HomePosture();
    }

    public void Disappoint() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = -375;
        servo[4] = 102;
        servo[5] = -54;
        servo[6] = 22;
        servo[9] = -11;
        servo[10] = -484;
        servo[7] = 534;

        setServos(servo, 500);

        servo[8] = 358;
        servo[4] = -33;
        servo[5] = -53;
        servo[6] = -125;
        servo[9] = 163;
        servo[10] = -300;
        servo[7] = 300;
        servo[11] = 219;
        servo[1] = -223;
        servo[12] = 96;
        servo[2] = -116;

        setServos(servo, 500);

        HomePosture();
    }

    public void Fearful() {
        servo[8] = 293;
        servo[4] = 75;
        servo[5] = -150;
        servo[6] = 925;
        servo[9] = -76;
        servo[10] = -701;
        servo[7] = 330;
        servo[11] = 129;
        servo[1] = 140;
        servo[12] = 91;
        servo[2] = 148;
        servo[13] = 4;
        servo[3] = 31;

        setServos(servo, 1000);

        HomePosture();
    }

    public void Hurt() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = -174;
        servo[4] = -58;
        servo[5] = 44;

        servo[6] = 1029;
        servo[9] = -35;
        servo[10] = -550;
        servo[7] = 401;

        servo[11] = 219;
        servo[1] = -223;
        servo[12] = 96;
        servo[2] = -116;

        setServos(servo, 500);

        for (int i = 0; i < 2; i++) {
            servo[8] = 194;
            servo[4] = -64;
            servo[5] = 49;

            setServos(servo, 100);

            servo[8] = -174;
            servo[4] = -58;
            servo[5] = 44;

            setServos(servo, 100);
        }

        HomePosture();
    }

    public void Sad() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = -277;
        servo[4] = 115;
        servo[5] = -204;

        servo[6] = -366;
        servo[9] = 349;
        servo[10] = -275;
        servo[7] = 275;

        setServos(servo, 500);

        servo[8] = 246;
        servo[4] = -53;
        servo[5] = -53;

        servo[6] = -279;
        servo[9] = -793;
        servo[10] = -335;
        servo[7] = 892;

        setServos(servo, 1000);

        servo[8] = -259;
        servo[4] = 0;
        servo[5] = -52;

        servo[6] = -293;
        servo[9] = -766;
        servo[10] = -320;
        servo[7] = 898;

        setServos(servo, 1000);

        HomePosture();
    }

    public void Sorry() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = 59;
        servo[4] = 117;
        servo[5] = -50;

        servo[6] = 236;
        servo[9] = -470;
        servo[10] = -619;
        servo[7] = 609;

        setServos(servo, 500);

        for (int i = 0; i < 2; i++) {
            servo[6] = 699;
            servo[9] = -407;
            servo[10] = -621;
            servo[7] = 758;

            setServos(servo, 250);

            servo[6] = 236;
            servo[9] = -470;
            servo[10] = -619;
            servo[7] = 609;

            setServos(servo, 250);
        }

        HomePosture();
    }

    public void Determined() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = -299;
        servo[4] = 8;
        servo[5] = 20;

        servo[6] = 226;
        servo[9] = 5;
        servo[10] = -495;
        servo[7] = 475;

        setServos(servo, 500);

        servo[10] = 118;

        setServos(servo, 250);

        HomePosture();
    }

    public void Cautious() {
        //Set all motors to current position
        ServoPosition[] pos = getServoPosition();
        for (int m = 0; m < servo.length-1; m++) {
            servo[m+1] = pos[m].pos;
        }

        servo[8] = -324;
        servo[4] = 119;
        servo[5] = -96;

        servo[6] = -255;
        servo[9] = 255;
        servo[10] = -185;
        servo[7] = 121;

        setServos(servo, 500);

        for (int i = 0; i < 2; i++) {
            servo[8] = 372;
            servo[4] = 122;
            servo[5] = -195;

            setServos(servo, 500);

            servo[8] = -324;
            servo[4] = 119;
            servo[5] = -96;

            setServos(servo, 500);
        }

        HomePosture();

        setServos(servo, 500);
    }
}
