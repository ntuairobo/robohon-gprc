package com.example.airobo.remotecontrol;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import com.example.airobo.remotecontrol.customize.ScenarioDefinitions;
import com.example.airobo.remotecontrol.util.ServoMotor;
import com.example.airobo.remotecontrol.util.VoiceUIManagerUtil;
import com.example.airobo.remotecontrol.util.VoiceUIVariableUtil;
import com.example.airobo.remotecontrol.util.VoiceUIVariableUtil.VoiceUIVariableListHelper;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.robohon_message.RoBoHoNMessageGrpc;
import io.grpc.robohon_message.desktop;
import io.grpc.robohon_message.robohon;
import jp.co.sharp.android.rb.projectormanager.ProjectorManagerServiceUtil;
import jp.co.sharp.android.robot.RobotServoManager;
import jp.co.sharp.android.robot.ServoPosition;
import jp.co.sharp.android.voiceui.VoiceUIManager;
import jp.co.sharp.android.voiceui.VoiceUIVariable;
import jp.co.sharp.android.rb.camera.FaceDetectionUtil;
import jp.co.sharp.android.rb.camera.ShootMediaUtil;
import jp.co.sharp.android.rb.rbdance.DanceUtil;

import static jp.co.sharp.android.rb.rbdance.DanceUtil.EXTRA_REQUEST_ID;
import static jp.co.sharp.android.rb.rbdance.DanceUtil.EXTRA_TYPE_ASSIGN;


public class MainActivity extends Activity implements MainActivityVoiceUIListener.MainActivityScenarioCallback {
    public static final String TAG = MainActivity.class.getSimpleName();
    public static final String ACTION_RESULT_FACE_DETECTION = "com.example.airobo.remotecontrol.action.RESULT_FACE_DETECTION";
    public static final String ACTION_RESULT_TAKE_PICTURE = "com.example.airobo.remotecontrol.action.RESULT_TAKE_PICTURE";
    public static final String ACTION_RESULT_REC_MOVIE = "com.example.airobo.remotecontrol.action.RESULT_REC_MOVIE";
    public static final String ACTION_RESULT_DANCE = "com.example.airobo.remotecontrol.action.RESULT_DANCE";
    private VoiceUIManager mVoiceUIManager = null;
    private MainActivityVoiceUIListener mMainActivityVoiceUIListener = null;
    private VoiceUIStartReceiver mVoiceUIStartReceiver = null;
    private HomeEventReceiver mHomeEventReceiver;
    private ProjectorEventReceiver mProjectorEventReceiver;
    private android.os.PowerManager.WakeLock mWakelock;
    private Object mLock = new Object();
    private boolean isProjected = false;
    private CameraResultReceiver mCameraResultReceiver;
    private DanceResultReceiver mDanceResultReceiver;

    private ManagedChannel channel;
    private EditText host_ip, host_port, duration;
    private String ip;
    private int port;
    private Button connect_button, disconnect_button;
    private Thread t;
    private String requestInfo; //Request sent to server
    private boolean commstarted = false;
    private boolean project_video = false;
    private boolean take_photo = false;
    ImageView photo;

    //initialize servo operation for moving RoBoHoN
    private RobotServoManager mRobotServoManager;
    int[] servo = {0,140,140,0,54,-54,-270,350,0,270,-350,-140,-140,0};
    private String name;
    private int age;
    private String gender;
    private String motion;
    private String speech;
    Queue<String> q = new LinkedList<>();
    ServoMotor sm;

    private SilenceDetector silenceDetector;
    private boolean isRecording = true;
    private int frequency = 8000;
    private int channelConfig = AudioFormat.CHANNEL_CONFIGURATION_MONO;
    private int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
    short threshold = 3000;
    private int prev_max, curr_max;
    private int buffer_size = 100 ;
    private boolean use_silence = false;
    private boolean get_reply = false;

    final MediaPlayer player = new MediaPlayer();
    private String video_id;
    int bufferSize = AudioRecord.getMinBufferSize(frequency, channelConfig, audioEncoding);
    AudioRecord record = new AudioRecord(MediaRecorder.AudioSource.MIC, frequency, channelConfig, audioEncoding, bufferSize);
    private WebView webView;
    List<String> prompts = new LinkedList<>();
    String photo_path = "";
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, "onCreate()");
        setContentView(R.layout.activity_main);
        setupTitleBar();
        mHomeEventReceiver = new HomeEventReceiver();
        IntentFilter filterHome = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(mHomeEventReceiver, filterHome);
        mVoiceUIStartReceiver = new VoiceUIStartReceiver();
        IntentFilter filter = new IntentFilter(VoiceUIManager.ACTION_VOICEUI_SERVICE_STARTED);
        registerReceiver(mVoiceUIStartReceiver, filter);

        setProjectorEventReceiver();

        mCameraResultReceiver = new CameraResultReceiver();
        IntentFilter filterCamera = new IntentFilter(ACTION_RESULT_TAKE_PICTURE);
        filterCamera.addAction(ACTION_RESULT_REC_MOVIE);
        filterCamera.addAction(ACTION_RESULT_FACE_DETECTION);
        registerReceiver(mCameraResultReceiver, filterCamera);

        mDanceResultReceiver = new DanceResultReceiver();
        IntentFilter filterDance = new IntentFilter(ACTION_RESULT_DANCE);
        registerReceiver(mDanceResultReceiver, filterDance);

        //For youtube videos (in landscape mode)
        webView = (WebView) findViewById(R.id.webView);
        webView.setVisibility(View.INVISIBLE);

        host_ip = (EditText) findViewById(R.id.ip);
        host_port = (EditText) findViewById(R.id.port);
        duration = (EditText) findViewById(R.id.duration);
        connect_button = (Button) findViewById(R.id.connect);
        disconnect_button = (Button) findViewById(R.id.disconnect);
        disconnect_button.setEnabled(false);
        requestInfo = "idle";
        photo = (ImageView) findViewById(R.id.photo);
        //prompts.add("我廳不清楚了。請你再說吧。");
        //prompts.add("誒？你剛剛說什麼呢？");
        //prompts.add("請你大聲一點在說一次");
        //prompts.add("不好意思，你可以慢一點再說一次嗎？");

        prompts.add("Sorry, I didn't understand what you said. Can you repeat?");
        prompts.add("Can you say it again?");
        prompts.add("Please say that again more slowly");

        mRobotServoManager = RobotServoManager.createInstance(this);

        /*//Immediately connect to server after starting app.
        commstarted = true;
        ip = host_ip.getText().toString();
        String port_str = host_port.getText().toString();
        port = Integer.valueOf(port_str);
        channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext(true).build();
        host_ip.setEnabled(false);
        host_port.setEnabled(false);
        duration.setEnabled(false);
        connect_button.setEnabled(false);
        disconnect_button.setEnabled(true);

        catchNewCommand();
        */
        connect_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commstarted = true;
                ip = host_ip.getText().toString();
                String port_str = host_port.getText().toString();
                port = Integer.valueOf(port_str);
                channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext(true).build();
                host_ip.setEnabled(false);
                host_port.setEnabled(false);
                duration.setEnabled(false);
                connect_button.setEnabled(false);
                disconnect_button.setEnabled(true);

                catchNewCommand();
            }
        });

        disconnect_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commstarted = false;
                t.interrupt();
                channel.shutdown();
                disconnect_button.setEnabled(false);
                host_ip.setEnabled(true);
                host_port.setEnabled(true);
                duration.setEnabled(true);
                connect_button.setEnabled(true);
            }
        });
    }

    //Thread for asking information from the desktop
    private void catchNewCommand() {
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!t.isInterrupted()) {
                        Thread.sleep(Integer.valueOf(duration.getText().toString()));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String info = getDesktopInfo(host_ip, "request");
                                if (info.equals("")) {
                                    t.interrupt();
                                }
                                else if (info.equals("terminate")){
                                    commstarted = false;
                                    t.interrupt();
                                    channel.shutdown();
                                    disconnect_button.setEnabled(false);
                                    host_ip.setEnabled(true);
                                    host_port.setEnabled(true);
                                    duration.setEnabled(true);
                                    connect_button.setEnabled(true);
                                    Log.d("server", "crashed");
                                }
                                else {
                                    String sentence = decodeString(info);
                                    if(sentence != null){
                                        executeAction(sentence);
                                        t.interrupt();
                                    }
                                }
                            }
                        });
                    }
                    Log.d("gRPC thread","Thread interrupted!");
                    catchNewCommand();


                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
    }

    private String decodeString(String info){
        String sentence = null;

        char info_type = info.charAt(0);
        if (info_type == 'n') {
            name = info.substring(1,info.length());
        }
        else if (info_type == 'a') {
            age = Integer.valueOf(info.substring(1,info.length()));
        }
        else if (info_type == 'g') {
            gender = info.substring(1,info.length());
        }
        else if (info_type == 'c'){
            int channel =  Integer.parseInt(info.substring(info.indexOf('p') + 1, info.indexOf('d')));
            int delay = Integer.parseInt(info.substring(info.indexOf('d') + 1, info.length()));
            switchPort(channel, delay);
        }
        else if (info_type == 'm') {
            motion = info.substring(1, info.length());
            //Separate metadata from sentence, meta is all before a #, sentence is after it.
            if (motion.contains("#")){
                String meta = motion.substring(0,motion.indexOf("#"));
                use_silence = meta.equals("prompt");
                get_reply = meta.equals("getreply") || meta.equals("doaction");
                sentence = motion.substring(motion.indexOf("#")+1, motion.length());
                //Check end of html speech to activate mic
                if(sentence.equals("@end")){
                    sentence = null;
                }
                //Activate mic if we expect speech
                if(get_reply){
                    try {
                        mVoiceUIManager.notifyEnableMic();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    try {
                        mVoiceUIManager.notifyDisableMic();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
            //Detach video URL
            else if (motion.contains("project_video")){
                sentence = "project_video";
                video_id = motion.substring(13);
            }
            else {
                sentence = motion;
            }
        }
        return sentence;
    }

    private void executeAction(String action) {
        //Codes will be used for actions that are not speech only.
        VoiceUIVariableListHelper helper = new VoiceUIVariableListHelper();

        switch (action) {
            case "standup":
                helper.addAccost(ScenarioDefinitions.ACC_STAND);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                break;
            case "sitdown":
                helper.addAccost(ScenarioDefinitions.ACC_SIT);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                break;
            case "a2101": //Gesture1: Stand still
                helper.addAccost(ScenarioDefinitions.ACC_STAND_STILL);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.HomePosture();
                break;
            case "a2111": //Gestures1: Hysterial
                helper.addAccost(ScenarioDefinitions.ACC_HYSTERICAL);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Hysterical();
                break;
            case "a2121": //Gestures1: Excited
                helper.addAccost(ScenarioDefinitions.ACC_EXCITED);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Excited();
                break;
            case "a2131": //Geatures1: Happy
                helper.addAccost(ScenarioDefinitions.ACC_HAPPY);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Happy();
                break;
            case "a2141": //Gestures1: Happy2
                helper.addAccost(ScenarioDefinitions.ACC_HAPPY2);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Happy2();
                break;
            case "a2151": //Gestures1: Interested
                helper.addAccost(ScenarioDefinitions.ACC_INTERESTED);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Interested();
                break;
            case "a2161": //Gestures1: Laugh
                helper.addAccost(ScenarioDefinitions.ACC_LAUGH);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Laugh();
                break;
            case "a2171": //Gestures1: Laugh2
                helper.addAccost(ScenarioDefinitions.ACC_LAUGH2);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Laugh2();
                break;
            case "a2181": //Gestures1: Proud
                helper.addAccost(ScenarioDefinitions.ACC_PROUD);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Proud();
                break;
            case "a2191": //Gestures1: Shy
                helper.addAccost(ScenarioDefinitions.ACC_SHY);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Shy();
                break;
            case "a2201": //Gestures2: Winner
                helper.addAccost(ScenarioDefinitions.ACC_WINNER);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Winner();
                break;
            case "a2211": //Gestures2: Angry
                helper.addAccost(ScenarioDefinitions.ACC_ANGRY);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Angry();
                break;
            case "a2221": //Gestures2: Disappoint
                helper.addAccost(ScenarioDefinitions.ACC_DISAPPOINT);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Disappoint();
                break;
            case "a2231": //Gestures2: Fearful
                helper.addAccost(ScenarioDefinitions.ACC_FEARFUL);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Fearful();
                break;
            case "a2241": //Gestures2: Hurt
                helper.addAccost(ScenarioDefinitions.ACC_HURT);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Hurt();
                break;
            case "a2251": //Gestures2: Sad
                helper.addAccost(ScenarioDefinitions.ACC_SAD);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Sad();
                break;
            case "a2261": //Gestures2: Sorry
                helper.addAccost(ScenarioDefinitions.ACC_SORRY);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Sorry();
                break;
            case "a2271": //Gestures2: Determined
                helper.addAccost(ScenarioDefinitions.ACC_DETERMINED);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Determined();
                break;
            case "a2281": //Gestures2: Cautious
                helper.addAccost(ScenarioDefinitions.ACC_CAUTIOUS);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                sm.Cautious();
                break;
            case "kungfu":
                sendBroadcast(getIntentForDance(EXTRA_TYPE_ASSIGN, 10));
                break;
            case "airguitar":
                sendBroadcast(getIntentForDance(EXTRA_TYPE_ASSIGN, 4));
                break;
            case "dance":
                sendBroadcast(getIntentForDance(EXTRA_TYPE_ASSIGN, 11));
                break;
            case "randomdance":
                sendBroadcast(getIntentForDance(EXTRA_TYPE_ASSIGN, (new Random()).nextInt(10)));
                break;
            case "play_music":
                playMusic();
                break;
            case "take_photo":
                sendBroadcast(getIntentForPhoto(false));
                break;
            case "project_photo":
                project_video = false;
                startService(getIntentForProjector("under"));
                break;
            case "close_photo":
                closeImage();
                break;
            case "project_video":
                project_video = true;
                helper.addAccost(ScenarioDefinitions.ACC_TURN_RIGHT);
                VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                break;
            case "raiseleft":
                moveArm("left","up");
                break;
            case "raiseright":
                moveArm("right","up");
                break;
            case "lowerleft":
                moveArm("left","down");
                break;
            case "lowerright":
                moveArm("right","down");
                break;
            default:
                speech = action;
                if (speech.contains("@")) {
                    String preHash = speech.substring(0,speech.indexOf("@"));
                    String postHash = "";
                    switch (speech.charAt(speech.indexOf("@") + 1)) {
                        // Replace name
                        case 'n':
                            postHash = speech.substring(speech.indexOf('@')+5,speech.length());
                            speech = preHash + name + postHash;
                            break;
                        // Replace age
                        case 'a':
                            postHash = speech.substring(speech.indexOf('@')+4,speech.length());
                            speech = preHash + age + postHash;
                            break;
                        //Replace gender
                        case 'g':
                            postHash = speech.substring(speech.indexOf('@')+7,speech.length());
                            speech = preHash + gender + postHash;
                            break;
                    }
                }
                Log.d("output", speech);
                say(speech);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v(TAG, "onResume()");

        //VoiceUIManagerのインスタンス取得.
        if (mVoiceUIManager == null) {
            mVoiceUIManager = VoiceUIManager.getService(getApplicationContext());
        }
        //MainActivityVoiceUIListener生成.
        if (mMainActivityVoiceUIListener == null) {
            mMainActivityVoiceUIListener = new MainActivityVoiceUIListener(this);
        }
        //VoiceUIListenerの登録.
        VoiceUIManagerUtil.registerVoiceUIListener(mVoiceUIManager, mMainActivityVoiceUIListener);

        //Scene有効化.
        VoiceUIManagerUtil.enableScene(mVoiceUIManager, ScenarioDefinitions.SCENE_COMMON);
        VoiceUIManagerUtil.enableScene(mVoiceUIManager, ScenarioDefinitions.SCENE01);

        if(!photo_path.isEmpty()){
            sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            Log.d("photo_path",photo_path);
            editor.putString("photo_path", photo_path);
            editor.commit();
        }

        //Retrieve shared preferences
        SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
        ip = prefs.getString("ip", "0");
        port = prefs.getInt("port", 0);
        video_id = prefs.getString("video_id", "0");
        project_video = prefs.getBoolean("project_video",false);
        photo_path = prefs.getString("photo_path","");

        //Disable mic by default
        /*try {
            mVoiceUIManager.notifyDisableMic();
        } catch (RemoteException e) {
            e.printStackTrace();
        }*/

        if(commstarted){
            Log.d("comm", "start");
            channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext(true).build();
            host_ip.setEnabled(false);
            host_port.setEnabled(false);
            duration.setEnabled(false);
            connect_button.setEnabled(false);
            disconnect_button.setEnabled(true);
            catchNewCommand();
        }
 }

    @Override
    public void onPause() {
        super.onPause();
        Log.v(TAG, "onPause()");

        //バックに回ったら発話を中止する.
        VoiceUIManagerUtil.stopSpeech();

        //VoiceUIListenerの解除.
        VoiceUIManagerUtil.unregisterVoiceUIListener(mVoiceUIManager, mMainActivityVoiceUIListener);

        //Scene無効化.
        VoiceUIManagerUtil.disableScene(mVoiceUIManager, ScenarioDefinitions.SCENE_COMMON);
        VoiceUIManagerUtil.disableScene(mVoiceUIManager, ScenarioDefinitions.SCENE01);

        //Save persistent data
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("ip", ip);
        editor.putInt("port", port);
        editor.putString("video_id", video_id);
        editor.putBoolean("project_video", project_video);
        editor.commit();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy()");

        //Enable mic
        try {
            mVoiceUIManager.notifyEnableMic();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        //ホームボタンの検知破棄.
        this.unregisterReceiver(mHomeEventReceiver);

        //VoiceUI再起動の検知破棄.
        this.unregisterReceiver(mVoiceUIStartReceiver);
        this.unregisterReceiver(mProjectorEventReceiver);
        this.unregisterReceiver(mCameraResultReceiver);
        this.unregisterReceiver(mDanceResultReceiver);

        //インスタンスのごみ掃除.
        mVoiceUIManager = null;
        mMainActivityVoiceUIListener = null;
        mProjectorEventReceiver = null;

        //Shut down gRPC server
        if(commstarted){
            commstarted = false;
            t.interrupt();
            channel.shutdown();
            disconnect_button.setEnabled(false);
            host_ip.setEnabled(true);
            host_port.setEnabled(true);
            duration.setEnabled(true);
            connect_button.setEnabled(true);
        }

        //Stop music player
        player.stop();
    }

    @Override
    public void onExecCommand(String command, List<VoiceUIVariable> variables) {
        Log.v(TAG, "onExecCommand() : " + command);
        Log.v(TAG, "silence: " + use_silence + " reply: " + get_reply);
        switch (command) {
            case ScenarioDefinitions.FUNC_END_APP:
                finish();
                break;
            case ScenarioDefinitions.FUNC_START_PROJECTOR:
                if(!isProjected) {
                    startService(getIntentForProjector("front"));
                }
                else{
                    webView.post(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl("javascript:\n" +
                                    "        var player = YT.get('playerId');\n" +
                                    "        player.playVideo();");
                        }
                    });
                }
                break;
            case ScenarioDefinitions.FUNC_END_PROJECTOR:
                if(isProjected) {
                    stopService(getIntentForProjector("front"));
                }
                break;
            case ScenarioDefinitions.FUNC_PAUSE_VIDEO:
                if(isProjected) {
                    webView.post(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl("javascript:\n" +
                                    "        var player = YT.get('playerId');\n" +
                                    "        player.pauseVideo();");
                        }
                    });
                }
                break;
            case ScenarioDefinitions.FUNC_FINISH_SAY:
                Log.d("comm",String.valueOf(commstarted));
                Log.d("proj",String.valueOf(isProjected));
                if(commstarted && !isProjected){
                    if(!get_reply) {
                        if (use_silence) {
                            Log.d("rec", "Attempting to detect silence!");
                            silenceDetector = new SilenceDetector();
                            silenceDetector.execute();
                            use_silence = false;
                        }
                        else {
                            if (!q.isEmpty()) executeAction(q.poll());
                            else {
                                Log.d("rec", "Receive next command");
                                requestInfo = "finish";
                                catchNewCommand();
                            }

                        }
                    }
                }
                break;
            case ScenarioDefinitions.FUNC_SPEECH_RECOG:
                final String response = VoiceUIVariableUtil.getVariableData(variables, "Lvcsr_BasicText");
                Log.d(TAG, response);
                if (commstarted && !isProjected){
                    if (!response.equals("ＶＯＩＣＥＰＦ＿ＥＲＲ＿ＲＥＣＯＧＮＩＺＥ＿ＲＥＴＲＹ")){
                        requestInfo = response;
                        catchNewCommand();
                    }
                    else
                    {
                        say(prompts.get((new Random()).nextInt(prompts.size())));
                    }
                }
                break;
            default:
                break;
        }
    }

    private void setupTitleBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setActionBar(toolbar);
    }

    private Intent getIntentForProjector(String direction) {
        Intent intent = new Intent();
        ComponentName componentName = new ComponentName(
                ProjectorManagerServiceUtil.PACKAGE_NAME,
                ProjectorManagerServiceUtil.CLASS_NAME);
        intent.putExtra(ProjectorManagerServiceUtil.EXTRA_PROJECTOR_OUTPUT, ProjectorManagerServiceUtil.EXTRA_PROJECTOR_OUTPUT_VAL_REVERSE);

        if (direction.equals("front")){
            intent.putExtra(ProjectorManagerServiceUtil.EXTRA_PROJECTOR_DIRECTION, ProjectorManagerServiceUtil.EXTRA_PROJECTOR_DIRECTION_VAL_FRONT);
        }
        else{
            intent.putExtra(ProjectorManagerServiceUtil.EXTRA_PROJECTOR_DIRECTION, ProjectorManagerServiceUtil.EXTRA_PROJECTOR_DIRECTION_VAL_UNDER);
        }

        intent.setComponent(componentName);
        return intent;
    }

    private void setProjectorEventReceiver() {
        Log.v(TAG, "setProjectorEventReceiver()");
        if (mProjectorEventReceiver == null) {
            mProjectorEventReceiver = new ProjectorEventReceiver();
        } else {
            return;
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ProjectorManagerServiceUtil.ACTION_PROJECTOR_PREPARE);
        intentFilter.addAction(ProjectorManagerServiceUtil.ACTION_PROJECTOR_START);
        //intentFilter.addAction(ProjectorManagerServiceUtil.ACTION_PROJECTOR_PAUSE);
        //intentFilter.addAction(ProjectorManagerServiceUtil.ACTION_PROJECTOR_RESUME);
        intentFilter.addAction(ProjectorManagerServiceUtil.ACTION_PROJECTOR_END);
        intentFilter.addAction(ProjectorManagerServiceUtil.ACTION_PROJECTOR_END_ERROR);
        intentFilter.addAction(ProjectorManagerServiceUtil.ACTION_PROJECTOR_END_FATAL_ERROR);
        intentFilter.addAction(ProjectorManagerServiceUtil.ACTION_PROJECTOR_TERMINATE);
        registerReceiver(mProjectorEventReceiver, intentFilter);
    }

    private void acquireWakeLock() {
        Log.v(TAG, "acquireWakeLock()");
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        synchronized (mLock) {
            if (mWakelock == null || !mWakelock.isHeld()) {
                mWakelock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK
                        | PowerManager.ACQUIRE_CAUSES_WAKEUP
                        | PowerManager.ON_AFTER_RELEASE, MainActivity.class.getName());
                mWakelock.acquire();
            }
        }
    }

    private void releaseWakeLock() {
        Log.v(TAG, "releaseWakeLock()");
        synchronized (mLock) {
            if (mWakelock != null && mWakelock.isHeld()) {
                mWakelock.release();
                mWakelock = null;
            }
        }
    }

    private Intent getIntentForFaceDetection(String swing) {
        Intent intent = new Intent(FaceDetectionUtil.ACTION_FACE_DETECTION_MODE);
        intent.setPackage(FaceDetectionUtil.PACKAGE);
        intent.putExtra(FaceDetectionUtil.EXTRA_REPLYTO_ACTION, ACTION_RESULT_FACE_DETECTION);
        intent.putExtra(FaceDetectionUtil.EXTRA_REPLYTO_PKG, getPackageName());
        intent.putExtra(FaceDetectionUtil.EXTRA_FACE_DETECTION_LENGTH, FaceDetectionUtil.EXTRA_FACE_DETECTION_LENGTH_NORMAL);
        intent.putExtra(FaceDetectionUtil.EXTRA_MOVE_HEAD, swing);
        return intent;
    }

    private Intent getIntentForPhoto(boolean facedetect) {
        Intent intent = new Intent(ShootMediaUtil.ACTION_SHOOT_IMAGE);
        intent.setPackage(ShootMediaUtil.PACKAGE);
        intent.putExtra(ShootMediaUtil.EXTRA_FACE_DETECTION, facedetect);
        intent.putExtra(ShootMediaUtil.EXTRA_REPLYTO_ACTION, ACTION_RESULT_TAKE_PICTURE);
        intent.putExtra(ShootMediaUtil.EXTRA_REPLYTO_PKG, getPackageName());
        //TODO 撮影対象指定する場合はContactIDを指定
        //intent.putExtra(ShootMediaUtil.EXTRA_CONTACTID, ShootMediaUtil.EXTRA_CONTACTID_OWNER);=
        return intent;
    }

    private void closeImage() {
        ImageView photo = (ImageView) findViewById(R.id.photo);
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout);
        photo.setVisibility(View.INVISIBLE);
        layout.setVisibility(View.VISIBLE);
        requestInfo = "finish";
        catchNewCommand();
    }

    private Intent getIntentForVideo(int time) {
        Intent intent = new Intent(ShootMediaUtil.ACTION_SHOOT_MOVIE);
        intent.setPackage(ShootMediaUtil.PACKAGE);
        intent.putExtra(ShootMediaUtil.EXTRA_MOVIE_LENGTH, time);
        intent.putExtra(ShootMediaUtil.EXTRA_REPLYTO_ACTION, ACTION_RESULT_REC_MOVIE);
        intent.putExtra(ShootMediaUtil.EXTRA_REPLYTO_PKG, getPackageName());
        return intent;
    }

    private Intent getIntentForDance(String type, int value) {
        Intent intent = new Intent(DanceUtil.ACTION_REQUEST_DANCE);
        intent.putExtra(DanceUtil.EXTRA_REPLYTO_ACTION, ACTION_RESULT_DANCE);
        intent.putExtra(DanceUtil.EXTRA_REPLYTO_PKG, getPackageName());
        intent.putExtra(DanceUtil.EXTRA_TYPE, type);
        if (type.equals(EXTRA_TYPE_ASSIGN)) {
            intent.putExtra(EXTRA_REQUEST_ID, value);
        }
        return intent;
    }

    private class HomeEventReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v(TAG, "Receive Home button pressed");
            // ホームボタン押下でアプリ終了する.
            finish();
        }
    }

    private class VoiceUIStartReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (VoiceUIManager.ACTION_VOICEUI_SERVICE_STARTED.equals(action)) {
                Log.d(TAG, "VoiceUIStartReceiver#onReceive():VOICEUI_SERVICE_STARTED");
                //VoiceUIManagerのインスタンス取得.
                mVoiceUIManager = VoiceUIManager.getService(getApplicationContext());
                if (mMainActivityVoiceUIListener == null) {
                    mMainActivityVoiceUIListener = new MainActivityVoiceUIListener(getApplicationContext());
                }
                //VoiceUIListenerの登録.
                VoiceUIManagerUtil.registerVoiceUIListener(mVoiceUIManager, mMainActivityVoiceUIListener);
            }
        }
    }

    private class ProjectorEventReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout);
            Log.v(TAG, "ProjectorEventReceiver#onReceive():" + intent.getAction());
            switch (intent.getAction()) {
                case ProjectorManagerServiceUtil.ACTION_PROJECTOR_PREPARE:
                    Log.d(TAG, "**********************Projector prepare");
                    break;
                case ProjectorManagerServiceUtil.ACTION_PROJECTOR_PAUSE:
                    Log.d(TAG, "**********************Projector pause");
                    break;
                case ProjectorManagerServiceUtil.ACTION_PROJECTOR_RESUME:
                    Log.d(TAG, "**********************Projector resume");
                    break;
                case ProjectorManagerServiceUtil.ACTION_PROJECTOR_START:
                    Log.d(TAG, "**********************Projector Start");

                    try {
                        mVoiceUIManager.notifyEnableMic();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    if (project_video){
                        Log.d("video", "start");
                        webView.setVisibility(View.VISIBLE);
                        layout.setVisibility(View.INVISIBLE);
                        loadVideo(webView, video_id);
                    }
                    else{
                        Log.d("photo", "start");
                        Log.d("photo_path", photo_path);
                        Bitmap bmp = BitmapFactory.decodeFile(photo_path);
                        photo.setImageBitmap(bmp);
                        photo.setVisibility(View.VISIBLE);
                        layout.setVisibility(View.INVISIBLE);

                    }

                    acquireWakeLock();
                    isProjected = true;
                    break;
                case ProjectorManagerServiceUtil.ACTION_PROJECTOR_END:
                    Log.d(TAG, "**********************Projector END");
                    releaseWakeLock();
                    isProjected = false;
                    webView.setVisibility(View.INVISIBLE);

                    //Open comms
                    commstarted = true;
                    channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext(true).build();
                    if(project_video){
                        //Face to the front
                        VoiceUIVariableListHelper helper = new VoiceUIVariableListHelper().addAccost(ScenarioDefinitions.ACC_TURN_LEFT);
                        VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
                    }
                    else{
                        photo.setVisibility(View.INVISIBLE);
                        layout.setVisibility(View.VISIBLE);
                        requestInfo = "finish";
                        catchNewCommand();
                    }

                    break;
                case ProjectorManagerServiceUtil.ACTION_PROJECTOR_END_FATAL_ERROR:
                    Log.d(TAG, "**********************Projector Fatal Error");
                    releaseWakeLock();
                    isProjected = false;
                    break;
                case ProjectorManagerServiceUtil.ACTION_PROJECTOR_END_ERROR:
                    Log.d(TAG, "**********************Projector Error");
                    releaseWakeLock();
                    isProjected = false;
                    break;
                case ProjectorManagerServiceUtil.ACTION_PROJECTOR_TERMINATE:
                    Log.d(TAG, "**********************Projector Terminate");
                    webView.onPause();
                    releaseWakeLock();
                    isProjected = false;
                    break;
                default:
                    break;
            }
        }
    }

    private class CameraResultReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "CameraResultReceiver#onReceive() : " + action);
            switch (action) {
                case ACTION_RESULT_FACE_DETECTION:
                    int result = intent.getIntExtra(FaceDetectionUtil.EXTRA_RESULT_CODE, FaceDetectionUtil.RESULT_CANCELED);
                    break;
                case ACTION_RESULT_TAKE_PICTURE:
                    result = intent.getIntExtra(ShootMediaUtil.EXTRA_RESULT_CODE, ShootMediaUtil.RESULT_CANCELED);
                    if(result == ShootMediaUtil.RESULT_OK) {
                        photo_path = intent.getStringExtra(ShootMediaUtil.EXTRA_PHOTO_TAKEN_PATH);

                        requestInfo = "finish";
                        catchNewCommand();
                    }
                    break;
                case ACTION_RESULT_REC_MOVIE:
                    result = intent.getIntExtra(ShootMediaUtil.EXTRA_RESULT_CODE, ShootMediaUtil.RESULT_CANCELED);
                    break;
                default:
                    break;
            }
        }
    }

    private class DanceResultReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int result = intent.getIntExtra(DanceUtil.EXTRA_RESULT_CODE, DanceUtil.RESULT_CANCELED);
            if (result == DanceUtil.RESULT_OK) {
                // 正常に完了した場合.
                requestInfo = "finish";
                catchNewCommand();
                int id = intent.getIntExtra(DanceUtil.EXTRA_RESULT_ID, -1);
            }
        }
    }

    private interface GrpcRunnable {
        /** Perform a grpcRunnable and return all the logs. */
        String run(RoBoHoNMessageGrpc.RoBoHoNMessageBlockingStub blockingStub, RoBoHoNMessageGrpc.RoBoHoNMessageStub asyncStub) throws Exception;
    }

    private static class GrpcTask extends AsyncTask<Void, Void, String> {
        private final GrpcRunnable grpcRunnable;
        private final ManagedChannel channel;
        private final WeakReference<MainActivity> activityReference;

        GrpcTask(GrpcRunnable grpcRunnable, ManagedChannel channel, MainActivity activity) {
            this.grpcRunnable = grpcRunnable;
            this.channel = channel;
            this.activityReference = new WeakReference<>(activity);
        }

        protected String waitForServer() {
            try {
                String info = grpcRunnable.run(RoBoHoNMessageGrpc.newBlockingStub(channel), RoBoHoNMessageGrpc.newStub(channel));
                Log.println(Log.INFO, "Result", "Success!\n" + info);
                return info;
            } catch (NetworkOnMainThreadException e){
                //TODO: Close app or something
                return "terminate";
            } catch (Exception e) {
                //StringWriter sw = new StringWriter();
                //PrintWriter pw = new PrintWriter(sw);
                //e.printStackTrace(pw);
                //pw.flush();
                Log.println(Log.INFO, "Result", "Failed... :\n"  + e);
                return " ";

            }
        }

        @Override
        protected String doInBackground(Void... nothing) {
            try {
                String info = grpcRunnable.run(RoBoHoNMessageGrpc.newBlockingStub(channel), RoBoHoNMessageGrpc.newStub(channel));
                Log.println(Log.INFO, "Result", "Success!\n" + info);
                return "Success!\n" + info;
            } catch (NetworkOnMainThreadException e){
                //TODO: Close app or something
                Log.i("sup","brah2");
                return "terminate";
            } catch (Exception e) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                pw.flush();
                Log.println(Log.INFO, "Result", "Failed... :\n" + sw);
                return "Failed... :\n" + sw;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            MainActivity activity = activityReference.get();
            if (activity == null) {
                return;
            }
        }
    }

    //Change to non-static function
    private class GetDesktopInfoRunnable implements GrpcRunnable {
        @Override
        public String run(RoBoHoNMessageGrpc.RoBoHoNMessageBlockingStub blockingStub, RoBoHoNMessageGrpc.RoBoHoNMessageStub asyncStub)
                throws Exception {
            return execMotion(blockingStub);
        }

        /** Blocking unary call example. Calls getFeature and prints the response. */
        private String execMotion(RoBoHoNMessageGrpc.RoBoHoNMessageBlockingStub blockingStub)
                throws StatusRuntimeException {
            robohon request = robohon.newBuilder().setInfoType(requestInfo).build();
            if (!requestInfo.equals("idle")) {
                requestInfo = "idle";
            }
            desktop info = blockingStub.requestInfo(request);
            //Return the information requested
            return info.getSentence();
        }
    }

    private String getDesktopInfo(View view, String item) {
        return new GrpcTask(new GetDesktopInfoRunnable(), channel, this).waitForServer();
    }

    //Silence detection code
    class SilenceDetector extends AsyncTask<Void, String, Void>{
        @Override
        protected Void doInBackground(Void... arg0) {
            isRecording = true;
            short[] buffer = new short[bufferSize];
            List<Boolean> vals = new ArrayList<>();
            record.startRecording();
            Log.d("rec","Started!");
            curr_max = 0;
            prev_max = 0;

            while(isRecording){
                //Detect silence
                record.read(buffer, 0, buffer.length);
                boolean res =searchThreshold(buffer, threshold);
                String temp = res?"sound":"silence";
                publishProgress(temp);
                vals.add(res);
                if (vals.size() == buffer_size) {
                    vals.remove(0);
                }
                prev_max = curr_max;
                curr_max = Collections.frequency(vals,true);
                if(curr_max < prev_max) {
                    curr_max = prev_max;
                }
                if (Collections.frequency(vals,true) == 0 && vals.size() >= buffer_size-1 ){
                    if (curr_max > 5){
                        Log.d("rec","Speech over!");
                        use_silence = false;
                        if(!q.isEmpty()) executeAction(q.poll());
                        else{
                            Log.d("rec","Receive next command");
                            requestInfo="finish";
                            catchNewCommand();
                        }
                    }
                    else{
                        Log.d("rec","Can you repeat!");
                        say(prompts.get((new Random()).nextInt(prompts.size())));
                        use_silence = true;
                        get_reply = false;
                    }
                    record.stop();
                    Log.d("rec","Finished recording!");
                    isRecording = false;
                }
            }
            return null;
        }
    }

    public boolean searchThreshold(short[]arr,short thr){
        int peakIndex;
        int count = 0;
        int arrLen=arr.length;
        for (peakIndex = 0; peakIndex < arrLen; peakIndex++){
            if ((arr[peakIndex] >= thr) || (arr[peakIndex] <= -thr)){
                count++;
            }
        }
        if (count >= 1){
            return true;
        }
        return false;
    }

    public void say(String speech){
        VoiceUIVariableListHelper helper = new VoiceUIVariableListHelper();
        VoiceUIVariableUtil.setVariableData(mVoiceUIManager,ScenarioDefinitions.MEM_SPEECH, speech);
        helper.addAccost(ScenarioDefinitions.ACC_HELLO);
        VoiceUIManagerUtil.updateAppInfo(mVoiceUIManager, helper.getVariableList(), true);
    }

    public void switchPort(int new_port, int delay){
        t.interrupt();
        channel.shutdown();
        Log.d("thread","sleep for " + delay/1000 + " seconds");
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        port = new_port;
        channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext(true).build();
        Log.d("thread","switched to port" + port);
        requestInfo = "switch";
        catchNewCommand();
    }

    public void playMusic(){
        if(player.isPlaying())
        {
            player.stop();
        }

        try {
            player.reset();
            AssetFileDescriptor afd;
            afd = getAssets().openFd("sound.mp3");
            player.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
            player.prepare();
            player.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int duration = player.getDuration();

        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        requestInfo = "finish";
        catchNewCommand();
    }


    private void moveArm(final String hand, final String position){
        // Set servo
        new Thread(new Runnable() {
            public void run() {
                ServoPosition lefthand = new ServoPosition();
                ServoPosition righthand = new ServoPosition();
                lefthand.servo_id = RobotServoManager.SERVO_SHOULDER_LEFT;
                righthand.servo_id = RobotServoManager.SERVO_SHOULDER_RIGHT;

                if(hand.equals("left")){
                    if(position.equals("up")) lefthand.pos = 1300;
                    else if(position.equals("down")) lefthand.pos = 0;
                }
                else if(hand.equals("right")){
                        if(position.equals("up")) righthand.pos = -1300;
                        else if(position.equals("down")) righthand.pos = 0;
                    }
                    else{
                    righthand.pos = -1300;
                }

                mRobotServoManager.startServoControl();
                mRobotServoManager.setServoPosition(new ServoPosition[]{lefthand, righthand},100);
                mRobotServoManager.stopServoControl();
            }
        }).start();

        requestInfo = "finish";
        catchNewCommand();
    }
    public void loadVideo(WebView view, String code) {
        Log.d("video_id", code);
        view.getSettings().setJavaScriptEnabled(true);
        view.getSettings().setMediaPlaybackRequiresUserGesture(false);
        view.setWebChromeClient(new WebChromeClient());
        String frameVideo = "<html><body style='margin:0px;padding:0px;'>\n" +
                "        <script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>\n" +
                "                var player;\n" +
                "        function onYouTubeIframeAPIReady()\n" +
                "        {player=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}\n" +
                "        function onPlayerReady(event){player.playVideo();}\n" +
                "        </script>\n" +
                "        <iframe id='playerId' type='text/html' width='100%' height='100%'\n" +
                "        src='https://www.youtube.com/embed/" + code + "?version=3&enablejsapi=1&autoplay=1' frameborder='0'>\n" +
                "        </body></html>";
        view.loadDataWithBaseURL("http://www.youtube.com", frameVideo, "text/html", "utf-8", null);

    }
}
