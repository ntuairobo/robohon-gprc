package com.example.airobo.remotecontrol.customize;

public class ScenarioDefinitions {

    public static final String TAG_SCENE = "scene";
    public static final String TAG_ACCOST = "accost";
    public static final String ATTR_TARGET = "target";
    public static final String ATTR_FUNCTION = "function";
    public static final String TAG_MEMORY_PERMANENT = "memory_p:";
    public static final String FUNC_END_APP = "end_app";
    protected static final String PACKAGE = "com.example.airobo.remotecontrol";
    public static final String TARGET = PACKAGE;
    public static final String SCENE_COMMON = PACKAGE + ".scene_common";
    public static final String SCENE01 = PACKAGE + ".scene01";

    //Projector variables
    public static final String FUNC_START_PROJECTOR = "start_projector";
    public static final String FUNC_END_PROJECTOR = "end_projector";
    public static final String FUNC_PAUSE_VIDEO = "pause_video";

    //Speech variables
    public static final String ACC_HELLO = ScenarioDefinitions.PACKAGE + ".hello.say";
    public static final String FUNC_FINISH_SAY = "finish_say";
    public static final String FUNC_SPEECH_RECOG = "speech_recog";

    //Motion variables
    public static final String ACC_STAND = ScenarioDefinitions.PACKAGE + ".motions.standup";
    public static final String ACC_SIT = ScenarioDefinitions.PACKAGE + ".motions.sitdown";
    public static final String ACC_BACK = ScenarioDefinitions.PACKAGE + ".motions.back";
    public static final String ACC_BELLY = ScenarioDefinitions.PACKAGE + ".motions.belly";
    public static final String ACC_TURN_RIGHT = ScenarioDefinitions.PACKAGE + ".motions.turn_right";
    public static final String ACC_TURN_LEFT = ScenarioDefinitions.PACKAGE + ".motions.turn_left";

    public static final String ACC_STAND_STILL = ScenarioDefinitions.PACKAGE + ".accost.stand_still";
    public static final String ACC_HYSTERICAL = ScenarioDefinitions.PACKAGE + ".accost.hysterical";
    public static final String ACC_EXCITED = ScenarioDefinitions.PACKAGE + ".accost.excited";
    public static final String ACC_HAPPY = ScenarioDefinitions.PACKAGE + ".accost.happy";
    public static final String ACC_HAPPY2 = ScenarioDefinitions.PACKAGE + ".accost.happy2";
    public static final String ACC_INTERESTED = ScenarioDefinitions.PACKAGE + ".accost.interested";
    public static final String ACC_LAUGH = ScenarioDefinitions.PACKAGE + ".accost.laugh";
    public static final String ACC_LAUGH2 = ScenarioDefinitions.PACKAGE + ".accost.laugh2";
    public static final String ACC_PROUD = ScenarioDefinitions.PACKAGE + ".accost.proud";
    public static final String ACC_SHY = ScenarioDefinitions.PACKAGE + ".accost.shy";
    public static final String ACC_WINNER = ScenarioDefinitions.PACKAGE + ".accost.winner";
    public static final String ACC_ANGRY = ScenarioDefinitions.PACKAGE + ".accost.angry";
    public static final String ACC_DISAPPOINT = ScenarioDefinitions.PACKAGE + ".accost.disappoint";
    public static final String ACC_FEARFUL = ScenarioDefinitions.PACKAGE + ".accost.fearful";
    public static final String ACC_HURT = ScenarioDefinitions.PACKAGE + ".accost.hurt";
    public static final String ACC_SAD = ScenarioDefinitions.PACKAGE + ".accost.sad";
    public static final String ACC_SORRY = ScenarioDefinitions.PACKAGE + ".accost.sorry";
    public static final String ACC_DETERMINED = ScenarioDefinitions.PACKAGE + ".accost.determined";
    public static final String ACC_CAUTIOUS = ScenarioDefinitions.PACKAGE + ".accost.cautious";

    //Speech accost and variable
    public static final String MEM_SPEECH = ScenarioDefinitions.TAG_MEMORY_PERMANENT + ScenarioDefinitions.PACKAGE + ".hello";
    public static final String ACC_END_APP = ScenarioDefinitions.PACKAGE + ".app_end.execute";
    private ScenarioDefinitions() {
    }
}
